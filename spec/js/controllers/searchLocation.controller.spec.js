var expect = chai.expect;

describe( 'The Search Location Controller', function() {
    var searchLocationController;
    var scope;

    beforeEach( module( 'WebGeoApp' ) );

    beforeEach( inject( function( $rootScope, $controller ) {
        searchLocationController = $controller( 'SearchLocationController', { $scope: scope } );
    } ) );

    it( 'should be available', function() {
        expect( searchLocationController ).to.exist;
    } );

    describe( 'when searching', function() {
        it( 'should tell the view a search is happening', function() {
            expect( searchLocationController.searchFilter.isSearching ).to.be.falsy;

            searchLocationController.locateWebsite();

            expect( searchLocationController.searchFilter.isSearching ).to.be.truthy;
        } );
    } );
} );
