var expect = chai.expect;

describe( 'The Map Controller', function() {
    var mapController;
    var scope;
    var modelLocation;

    beforeEach( module( 'WebGeoApp' ) );

    beforeEach( inject( function( $rootScope, $controller ) {
        scope = $rootScope.$new();
        mapController = $controller( 'MapController', { $scope: scope } );
    } ) );

    beforeEach( inject( function( locationModel ) {
        modelLocation = locationModel;
    } ) );

    it( 'should be available', function() {
        expect( mapController ).to.exist;
    } );

    it( 'should update the map with the current location', function() {
        expect( mapController.centerMapOnCurrentLocation() ).to.not.throw;
    } );

    it('should update the map after a search', function() {
        modelLocation.model.searchedLocations.push( {
            'as': 'AS26615 Tim Celular S.A.',
            'city': 'São Paulo',
            'country': 'Brazil',
            'countryCode': 'BR',
            'isp': 'Tim Celular S.A.',
            'lat': -23.5475,
            'lon': -46.6361,
            'org': 'Tim Celular S.A.',
            'query': '179.35.184.2',
            'region': 'SP',
            'regionName': 'Sao Paulo',
            'status': 'success',
            'timezone': 'America/Sao_Paulo',
            'zip': ''
        } );

        expect( scope.$root.$broadcast( 'websiteLocated' ) ).to.not.throw;
    } );
} );
