var expect = chai.expect;

describe( 'The Estimated Location Controller', function() {
    var estimatedLocationController;
    var scope;

    beforeEach( module( 'WebGeoApp' ) );

    beforeEach( inject( function( $rootScope, $controller ) {
        estimatedLocationController = $controller( 'EstimatedLocationController', { $scope: scope } );
    } ) );

    it( 'should be available', function() {
        expect( estimatedLocationController ).to.exist;
    } );
} );
