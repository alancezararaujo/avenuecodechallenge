var expect = chai.expect;

describe( 'The Info Button Directive', function() {
    var compile;
    var rootScope;

    beforeEach( module( 'WebGeoApp' ) );

    beforeEach( inject( function( $compile, $rootScope ) {
        compile = $compile;
        rootScope = $rootScope;
    } ) );

    it( 'should add a button', function() {
        var element = compile( '<span info-button field="\'IP\'"></span>' )( rootScope );

        rootScope.$digest();

        expect( element.html() ).to.contain( "<button " );
    } );

    it( 'should alert a message on click', function( done ) {
        rootScope.EstimatedLocationVM = {
            lastUpdateTime: new Date(),
            currentLocation: {
                isp: ''
            }
        };

        var element = compile( '<span info-button field="\'IP\'"></span>' )( rootScope );

        rootScope.$digest();

        window.alert = function(){};

        element.triggerHandler( 'click' );

        done();
    } );
} );
