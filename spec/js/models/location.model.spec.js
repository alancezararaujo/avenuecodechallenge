var expect = chai.expect;

describe( 'The Location Model', function() {
    var modelLocation;
    var currentLocation = {
        'as':'AS26615 Tim Celular S.A.',
        'city':'São Paulo',
        'country':'Brazil',
        'countryCode':'BR',
        'isp':'Tim Celular S.A.',
        'lat':-23.5475,
        'lon':-46.6361,
        'org':'Tim Celular S.A.',
        'query':'179.35.184.2',
        'region':'SP',
        'regionName':'Sao Paulo',
        'status':'success',
        'timezone':'America/Sao_Paulo',
        'zip':''
    };
    var $$httpBackend;

    beforeEach( module( 'WebGeoApp' ) );

    beforeEach( inject( function( locationModel ) {
        modelLocation = locationModel;
    } ) );

    beforeEach( inject( function( $httpBackend ) {
        $$httpBackend = $httpBackend;

        $$httpBackend.whenGET( 'http://ip-api.com/json' )
            .respond( 200, currentLocation );
    } ) );

    it( 'should be available', function() {
        expect( modelLocation ).to.exist;
    } );

    it( 'should provide an array of searched locations', function() {
        expect( modelLocation.model.searchedLocations ).to.be.an( 'array' );
        expect( modelLocation.model.searchedLocations ).to.be.empty;
    } );

    it( 'should provide an object to store the current location data', function() {
        expect( modelLocation.model.currentLocation ).to.be.an( 'object' );
        expect( modelLocation.model.currentLocation ).to.be.empty;
    } );

    describe( 'when reseting the current location', function() {
        it( 'should clear the current location data', function() {
            angular.copy( currentLocation, modelLocation.model.currentLocation );

            expect( modelLocation.model.currentLocation ).to.not.be.empty;

            modelLocation.resetCurrentLocation();

            expect( modelLocation.model.currentLocation ).to.be.empty;
        } );
    } );

    describe( 'when searching current location', function() {
        it( 'should update the model with the current location data', function() {
            expect( modelLocation.model.currentLocation ).to.be.empty;

            modelLocation.getCurrentLocation();

            $$httpBackend.flush();

            expect( modelLocation.model.currentLocation ).to.not.be.empty;
        } );
    } );
} );
