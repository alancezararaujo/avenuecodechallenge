var gulp        = require( 'gulp' );
var browserSync = require( 'browser-sync' ).create();
var jshint      = require( 'gulp-jshint' );

gulp.task( 'html', function() {
    return gulp.src( './**/*.html' );
} );

gulp.task( 'js', function() {
    return gulp.src( [ './js/**/*.js', '!./js/libs/**/*.js' ] )
        .pipe( jshint() )
        .pipe( jshint.reporter( 'jshint-stylish' ) );
} );

gulp.task( 'server', function() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    } );
} );

gulp.task( 'default', [ 'server' ], function() {
    gulp.watch( './js/**/*.js', [ 'js' ] ).on( 'change', browserSync.reload );
    gulp.watch( './**/*.html' ).on( 'change', browserSync.reload );
} );
