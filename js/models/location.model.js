( function() {
    'use strict';

    function LocationModel( $http ) {
        var model = {
            currentLocation: {},
            searchedLocations: [],
            lastUpdateTime: new Date()
        };

        function resetCurrentLocation() {
            angular.copy( {}, model.currentLocation );
        }

        function getCurrentLocation( successCallback ) {
            $http.get( 'http://ip-api.com/json' )
                .success( function( location ) {
                    angular.copy( location, model.currentLocation );
                    angular.copy( new Date(), model.lastUpdateTime );

                    successCallback && successCallback();
                } )
                .error( function( error, status ) {
                    alert( 'It seems we are having some problem to contact the server. Try again later.' );
                } );
        }

        function searchWebSiteLocation( hostname, successCallback, finallyCallback ) {
            $http.get( 'http://freegeoip.net/json/' + hostname )
                .success( function( location ) {
                    location.hostname = hostname;
                    model.searchedLocations.push( location );

                    successCallback && successCallback();
                } )
                .error( function( error, status ) {
                    if ( status === 404 ) {
                        alert( 'The website was not found.' );
                        return;
                    }

                    alert( 'It seems we are having some problem to contact the server. Try again later.' );
                } )
                .finally( finallyCallback );
        }

        return {
            // MODEL/ENTITY
            model: model,

            // METHODS
            resetCurrentLocation: resetCurrentLocation,
            getCurrentLocation: getCurrentLocation,
            searchWebSiteLocation: searchWebSiteLocation
        };
    }

    LocationModel.$inject = [ '$http' ];

    angular
        .module( 'WebGeoApp' )
        .factory( 'locationModel', LocationModel );
} )();
