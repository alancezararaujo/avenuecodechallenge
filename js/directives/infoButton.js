( function( angular ) {
    'use strict';

    function link( scope, $element ) {
        $element.on( 'click', function() {
            var lastUpdate = scope.$parent.EstimatedLocationVM.lastUpdateTime;
            var isp = scope.$parent.EstimatedLocationVM.currentLocation.isp;

            alert( 'This is your ' + scope.field + ' from ISP ' + isp + ' at ' + lastUpdate );
        } );
    }

    function infoButton() {
        return {
            restrict: 'AEC',
            link: link,
            transclude: true,
            scope: {
                field: '=field'
            },
            template: [
                '<button class="btn btn-xs">',
                '    <span class="sr-only">Learn more</span>',
                '    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>',
                '</button>'
            ].join( '' )
        };
    }

    angular
        .module( 'WebGeoApp' )
        .directive( 'infoButton', infoButton );

} )( angular );
