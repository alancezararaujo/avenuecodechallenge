( function( angular, google ) {
    'use strict';

    function MapController( $scope, locationModel, customMapStyle ) {

        /**
         * List with all websites searched by the user.
         * @type {array}
         */
        var markers = [];

        /**
         * Cache the marker with the current location.
         * @type {object|google.maps.Marker}
         */
        var currentLocationMarker = {};

        var mapOptions = {
            center: new google.maps.LatLng( 0, 0 ),
            zoom: 2,
            panControl: true,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },
            scaleControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map( document.getElementById( 'map' ), mapOptions );

        map.mapTypes.set( 'WebGeoMapStyle', new google.maps.StyledMapType( customMapStyle, 'WebGeoMapStyle' ) );
        map.setMapTypeId( 'WebGeoMapStyle' );

        var markerIcon = {
            path: 'M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z',
            scale: 0.5,
            strokeWeight: 2,
            fillColor: 'black',
            fillOpacity: 1
        };

        function centerMapOnCurrentLocation() {
            removeCurrentLocation();

            var longitude = locationModel.model.currentLocation.lon;
            var latitude = locationModel.model.currentLocation.lat;

            centerMap( latitude, longitude );

            currentLocationMarker = new google.maps.Marker( {
                position: new google.maps.LatLng( latitude, longitude ),
                map: map,
                animation: google.maps.Animation.DROP,
                title: 'Current location',
                icon: markerIcon
            } );
        }

        function removeCurrentLocation() {
            currentLocationMarker.setMap && currentLocationMarker.setMap( null );
        }

        function centerMap( latitude, longitude ) {
            map.setCenter( new google.maps.LatLng( latitude, longitude ) );
            map.setZoom( 12 );
        }

        function addMarker( latitude, longitude, title, icon ) {
            var marker = new google.maps.Marker( {
                position: new google.maps.LatLng( latitude, longitude ),
                map: map,
                animation: google.maps.Animation.DROP,
                title: title,
                icon: icon || markerIcon
            } );

            markers.push( marker );
        }

        $scope.$on( 'currentLocationUpdated', function() {
            centerMapOnCurrentLocation();
        } );

        $scope.$on( 'currentLocationReset', function() {
            removeCurrentLocation();
        } );

        $scope.$on( 'websiteLocated', function() {
            var locations = locationModel.model.searchedLocations;
            var website = locations[ locations.length - 1 ];

            centerMap( website.latitude, website.longitude );
            addMarker( website.latitude, website.longitude, website.hostname );
        } );

        this.centerMapOnCurrentLocation = centerMapOnCurrentLocation;
    }

    MapController.$inject = [ '$scope', 'locationModel', 'customMapStyle' ];

    angular
        .module( 'WebGeoApp' )
        .controller( 'MapController', MapController );
} )( angular, google );
