( function( angular ) {
    'use strict';

    function SearchLocationController( $scope, locationModel ) {
        var searchFilter = {
            hostname: '',
            isSearching: false
        };

        function locateWebsite() {
            searchFilter.isSearching = true;

            locationModel.searchWebSiteLocation( searchFilter.hostname, function() {
                $scope.$root.$broadcast( 'websiteLocated' );
            }, function() {
                searchFilter.isSearching = false;
            } );
        }

        angular.extend( this, {
            searchFilter: searchFilter,
            locateWebsite: locateWebsite
        } );
    }

    SearchLocationController.$inject = [ '$scope', 'locationModel' ];

    angular
        .module( 'WebGeoApp' )
        .controller( 'SearchLocationController', SearchLocationController );
} )( angular );
