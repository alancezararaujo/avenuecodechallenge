( function( angular ) {
    'use strict';

    function EstimatedLocationController( $scope, locationModel ) {
        var currentLocation = locationModel.model.currentLocation;
        var lastUpdateTime = locationModel.model.lastUpdateTime;

        function getCurrentLocation() {
            locationModel.getCurrentLocation( function() {
                $scope.$root.$broadcast( 'currentLocationUpdated' );
            } );
        }

        function resetCurrentLocation() {
            locationModel.resetCurrentLocation();
            $scope.$root.$broadcast( 'currentLocationReset' );
        }

        angular.extend( this, {
            // VIEW DATA
            currentLocation: currentLocation,
            lastUpdateTime: lastUpdateTime,

            // METHODS
            getCurrentLocation: getCurrentLocation,
            resetCurrentLocation: resetCurrentLocation
        } );
    }

    EstimatedLocationController.prototype.setCurrentLocation =

    EstimatedLocationController.$inject = [ '$scope', 'locationModel' ];

    angular
        .module( 'WebGeoApp' )
        .controller( 'EstimatedLocationController', EstimatedLocationController );
} )( angular );
