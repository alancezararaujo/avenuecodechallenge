# Avenue Code UI Challenge - Part I #

## WebGeo ##
**WebGeo** is a web application, which allows the user to find the location of any website on a map.

## Development ##

### Environment Configuration ###
Before you run the application, be sure to have installed the following dependencies:

* Node.js
* NPM
* JSHint
* BrowserSync

### Technologies ###
Before you start, you must to know the following technologies:

#### HTML ####
You have to understand the minimal about [semantic code](https://en.wikipedia.org/wiki/Semantic_HTML)
and [accessible HTML](http://www.webstandards.org/learn/tutorials/accessible-forms/beginner/).

#### CSS ####
The CSS on this page is quite simple and small. So pay attention to
write [fast code](https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Writing_efficient_CSS).

#### JavaScript ####
The JavaScript part is organized on MVC Architecture, using [angular.js](https://angularjs.org).

To keep the high quality, pay attention to the following points:

* Keep an eye on the terminal. The JSHint is helping you with great tips and warning;
* Write semantic, easy and small code;
* Review! If your code is not clear, take a second look and you'll probably make it better;

### Installing the dependencies ###
From the terminal, you must to execute the following command:

```
npm i && bower install
```

### Running the Application ###
After installing all dependencies, you may run the application from the terminal:

```
npm start
```

### Test ###
With the application running, you can hit "http://localhost:3000/spec/spec.html" to verify the tests.
